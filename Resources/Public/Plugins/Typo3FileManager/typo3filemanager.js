/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (http://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

tinymce.PluginManager.requireLangPack('typo3filemanager', 'en_GB,de');

/**
 * Minified with: http://lisperator.net/uglifyjs
 */
var plugin = tinymce.PluginManager.add(
	'typo3filemanager', function(editor, url) {
		/**
		 * Builds a module url
		 *
		 * Taken from the htmlarea code
		 *
		 * @param {string} modulePath
		 * @param {array} parameters
		 * @return {string}
		 */
		var makeUrlFromModulePath = function(modulePath, parameters) {
			var editorId = editor.settings.editornumber;
			return modulePath + (modulePath.indexOf("?") === -1 ? "?" : "&") +
				'&RTEtsConfigParams=' + RTEarea[editorId].RTEtsConfigParams +
				'&sys_language_content=' + RTEarea[editorId].sys_language_content +
				'&contentTypo3Language=' + RTEarea[editorId].typo3ContentLanguage +
				'&editorNo=' + RTEarea[editorId].number + (parameters ? parameters : '');
		};

		/**
		 * Opens the image dialog
		 *
		 * @return {void}
		 */
		var openImageDialog = function() {
			var selectedElement = editor.selection.getNode();
			var image = editor.dom.getParent(selectedElement, 'img[src]');

			var additionalParameter = '';
			if (image) {
				additionalParameter = '&act=image&fileUid=' + image.getAttribute('data-htmlarea-file-uid');
			}

			editor.windowManager.open(
				{
					title: 'TYPO3 Image',
					url: makeUrlFromModulePath(RTEarea[editor.settings.editornumber].buttons.image.pathImageModule, additionalParameter),
					width: 800,
					height: 600,
					buttons: [
						{
							text: 'Close',
							onclick: 'close'
						}
					]
				}
			);
		};

		/**
		 * Opens the link dialog
		 *
		 * @return {void}
		 */
		var openLinkDialog = function() {
			var selectedElement = editor.selection.getNode();
			var element = editor.dom.getParent(selectedElement, 'a[href]');

			var additionalParameter = '';
			if (element) {
				additionalParameter = '&curUrl[url]=' + encodeURIComponent(element.getAttribute('href'));
				if (element.target) {
					additionalParameter += '&curUrl[target]=' + encodeURIComponent(element.target);
				}
				if (element.className) {
					additionalParameter += '&curUrl[class]=' + encodeURIComponent(element.className);
				}
				if (element.title) {
					additionalParameter += '&curUrl[title]=' + encodeURIComponent(element.title);
				}
			}

			editor.windowManager.open(
				{
					title: 'TYPO3 Link',
					url: makeUrlFromModulePath(RTEarea[editor.settings.editornumber].buttons.link.pathLinkModule, additionalParameter),
					width: 800,
					height: 600,
					buttons: [
						{
							text: 'Close',
							onclick: 'close'
						}
					]
				}
			);
		};

		// add the buttons
		editor.addButton(
			'typo3link', {
				title: 'TYPO3 Link',
				icon: 'link',
				shortcut: 'Ctrl+K',
				onclick: openLinkDialog,
				stateSelector: 'a[href]'
			}
		);

		editor.addButton(
			'unlink', {
				title: 'Unlink',
				icon: 'unlink',
				shortcut: 'Ctrl+M',
				cmd: 'unlink',
				stateSelector: 'a[href]'
			}
		);

		editor.addButton(
			'typo3image', {
				title: 'TYPO3 Image',
				icon: 'image',
				shortcut: 'Ctrl+L',
				onclick: openImageDialog,
				stateSelector: 'img[src]'
			}
		);

		// add the menu entries
		editor.addMenuItem(
			'unlink', {
				text: 'Unlink',
				context: 'insert',
				prependToContext: true,
				shortcut: 'Ctrl+M',
				icon: 'unlink',
				cmd: 'unlink',
				stateSelector: 'a[href]'
			}
		);

		editor.addMenuItem(
			'typo3link', {
				text: 'TYPO3 Link',
				context: 'insert',
				prependToContext: true,
				shortcut: 'Ctrl+K',
				icon: 'link',
				onclick: openLinkDialog,
				stateSelector: 'a[href]'
			}
		);

		editor.addMenuItem(
			'typo3image', {
				text: 'TYPO3 Image',
				context: 'insert',
				prependToContext: true,
				shortcut: 'Ctrl+L',
				icon: 'image',
				onclick: openImageDialog,
				stateSelector: 'img[src]'
			}
		);

		// initialize the shortcuts
		editor.addShortcut('Ctrl+K', '', openLinkDialog);
		editor.addShortcut('Ctrl+M', '', 'unlink');
		editor.addShortcut('Ctrl+L', '', openImageDialog);
	}
);

/**
 * Renders a link
 *
 * @param {string} href
 * @param {string} target
 * @param {string} cssClass
 * @param {string} title
 * @param {object} additionalValues currently unused
 * @return {void}
 */
plugin.createLink = function(href, target, cssClass, title, additionalValues) {
	var linkAttrs = {
		href: href,
		target: target ? target : null,
		class: cssClass ? cssClass : null,
		title: title ? title : null,
		'data-htmlarea-external': null
	};

	for (var index in additionalValues) {
		if (additionalValues.hasOwnProperty(index)) {
			linkAttrs[index] = additionalValues[index];
		}
	}

	var selectedElement = tinymce.activeEditor.selection.getNode();
	var element = tinymce.activeEditor.dom.getParent(selectedElement, 'a[href]');
	tinymce.activeEditor.focus();
	if (element) {
		tinymce.activeEditor.dom.setAttribs(element, linkAttrs);
	} else {
		tinymce.activeEditor.execCommand('mceInsertLink', false, linkAttrs);
	}
	tinymce.activeEditor.selection.collapse();
	tinymce.activeEditor.undoManager.add();

	tinymce.activeEditor.windowManager.getWindows()[0].close();
};

/**
 * Unlinks the current selection
 *
 * @return {void}
 */
plugin.unLink = function() {
	tinymce.activeEditor.execCommand('unlink');
	tinymce.activeEditor.windowManager.getWindows()[0].close();
};

/**
 * Renders an image
 *
 * @return {void}
 */
plugin.insertImage = function(image) {
	tinymce.activeEditor.focus();
	tinymce.activeEditor.selection.setContent(image);
	tinymce.activeEditor.undoManager.add();

	tinymce.activeEditor.windowManager.getWindows()[0].close();
};

/**
 * Just returns null
 *
 * @returns {null}
 */
plugin.getButton = function() {
	return null;
};

/**
 * Returns the current selected image if any
 *
 * @return {Element}
 */
plugin.getSelectedImage = function() {
	var selectedElement = tinymce.activeEditor.$(tinymce.activeEditor.selection.getNode());
	return (selectedElement.length ? selectedElement.closest('img[src]') : {});
};

/**
 * Closes the current open dialog
 *
 * @return {void}
 */
plugin.close = function() {
	tinymce.activeEditor.windowManager.getWindows()[0].close();
};