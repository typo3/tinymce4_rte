<?php

$EM_CONF[$_EXTKEY] = [
	'title' => 'TinyMCE4 as TYPO3 RTE',
	'description' => 'RTE integration of TinyMCE for TYPO3.',
	'category' => 'be',
	'version' => '3.0.3',
	'state' => 'stable',
	'uploadfolder' => FALSE,
	'createDirs' => '',
	'clearcacheonload' => FALSE,
	'author' => 'Stefan Galinski',
	'author_email' => 'stefan@sgalinski.de',
	'author_company' => 'sgalinski Internet Services',
	'autoload' => 
        [
            'psr-4' => ['SGalinski\\Tinymce4Rte\\' => 'Classes']
        ],
	'constraints' =>
		[
			'depends' =>
				[
					'php' => '7.0.0-7.2.99',
					'typo3' => '7.6.0-8.7.99',
					'tinymce' => '5.0.0-5.0.99',
				],
			'conflicts' =>
				[
					'ch_rterecords' => '',
					'linkhandler' => '',
					'rtehtmlarea' => '',
					'rte_ckeditor' => '',
					'rte_tinymce' => '',
					'tinyrteru' => '',
					'tinyrte' => '',
				],
			'suggests' =>
				[],
		],
];