<?php

namespace SGalinski\Tinymce4Rte\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class SGalinski\SgRoutes\Service\LicensingService
 */
class LicensingService {
	/**
	 * Licensing Service Url
	 */
	const URL = 'https://www.sgalinski.de/?eID=sgLicensing';

	/**
	 * The key name of the uc array inside of the backend user.
	 */
	const BACKEND_USER_UC_KEY = 'tinymce4_rte_pinged';

	/**
	 * Licensing Service Url
	 */
	const EXTENSION_KEY = 'tinymce4_rte';

	/** @var bool|NULL */
	private static $isLicenseKeyValid;

	/**
	 * @return boolean
	 */
	public static function checkKey(): bool {
		if (static::$isLicenseKeyValid === NULL) {
			static::$isLicenseKeyValid = FALSE;
			$configuration = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][self::EXTENSION_KEY], [FALSE]);
			if (isset($configuration['key']) && $key = trim($configuration['key'])) {
				static::$isLicenseKeyValid = (bool) preg_match('/^([A-Z\d]{6}-?){4}$/', $key);
			}
		}

		return static::$isLicenseKeyValid;
	}

	/**
	 * Licensing Service ping
	 *
	 * @param boolean $returnUrl
	 * @return string
	 */
	public static function ping($returnUrl = FALSE): string {
		try {
			$configuration = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][self::EXTENSION_KEY], [FALSE]);
			$key = '';
			if (isset($configuration['key'])) {
				$key = trim($configuration['key']);
			}
			$params = [
				'extension' => self::EXTENSION_KEY,
				'host' => GeneralUtility::getIndpEnv('HTTP_HOST'),
				// @todo Enable this, when inserting a licensing.
				//'key' => $key
			];
			$params = http_build_query($params);
			$pingUrl = self::URL;
			$pingUrl .= $params !== '' ? (strpos($pingUrl, '?') === FALSE ? '?' : '&') . $params : '';
			if ($returnUrl) {
				return $pingUrl;
			}

			GeneralUtility::getUrl($pingUrl);
		} catch (\Exception $exception) {
		}

		return '';
	}

	/**
	 * Generates a random password string based on the configured password policies.
	 *
	 * @param ServerRequestInterface $request
	 * @param ResponseInterface $response
	 * @return ResponseInterface
	 * @throws \InvalidArgumentException
	 */
	public function ajaxPing(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface {
		/** @var BackendUserAuthentication $backendUser */
		$backendUser = $GLOBALS['BE_USER'];
		$backendUserUC = $backendUser->uc;
		if ($backendUser && !isset($backendUserUC[self::BACKEND_USER_UC_KEY])) {
			$backendUserUC[self::BACKEND_USER_UC_KEY] = TRUE;
			$backendUser->writeUC($backendUserUC);
			self::ping();
		}
		return $response;
	}
}
