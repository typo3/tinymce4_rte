<?php

namespace SGalinski\Tinymce4Rte\Utility;

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\VersionNumberUtility;

/**
 * Helper class to detect the used TYPO3 version.
 */
class VersionUtility {
	/**
	 * Returns true if the current version ts TYPO3 6.2.
	 *
	 * @return bool
	 */
	public static function isVersion62() {
		$versionNumber = self::getVersion();
		return ($versionNumber >= 6002000 && $versionNumber < 7000000);
	}

	/**
	 * Returns true if the current version ts TYPO3 7.6 and less version 8
	 *
	 * @return bool
	 */
	public static function isVersion76() {
		$versionNumber = self::getVersion();
		return ($versionNumber >= 7006000 && $versionNumber < 8000000);
	}

	/**
	 * Returns true if the current version ts TYPO3 7.6 or later
	 *
	 * @return bool
	 */
	public static function isVersion76OOrHigher() {
		return (self::getVersion() >= 7006000);
	}

	/**
	 * Returns true if the current version ts TYPO3 8.7 or later
	 *
	 * @return bool
	 */
	public static function isVersion870OrHigher() {
		return (self::getVersion() >= 8007000);
	}

	/**
	 * Returns the current version as an integer.
	 *
	 * @return int
	 */
	protected static function getVersion() {
		return VersionNumberUtility::convertVersionNumberToInteger(
			VersionNumberUtility::getNumericTypo3Version()
		);
	}
}
