# tinymce4_rte

Augments the tinymce-extension with a more powerful and userfriendly interface.

## Information regarding the image dialog

Currently the image browser isn't integrated well in TYPO3 anymore. The wizard dialog can be added, but it crashed
with errors. Also the code in TYPO3 itself is broken and incompatible currently. We will integrate the wizard again if
the dialog will be reintegrated in rtehtmlarea again.

## FAQ

### How can I stop, that my custom classes for the table tag are removed in the frontend?

Just add these two lines into your TypoScript and adapt the class at the end of each line.
This will prevent the deletion of the custom classes from the table tag within the frontend.

```
lib.parseFunc_RTE.externalBlocks.table.stdWrap.HTMLparser.tags.table.fixAttrib.class.default = responsive 
lib.parseFunc_RTE.externalBlocks.table.stdWrap.HTMLparser.tags.table.fixAttrib.class.list = responsive
```
