<?php

if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

// activate the RTE by default
if (!$GLOBALS['TYPO3_CONF_VARS']['BE']['RTEenabled']) {
	$GLOBALS['TYPO3_CONF_VARS']['BE']['RTEenabled'] = 1;
}

// Register FormEngine node type resolver hook to render RTE in FormEngine if enabled
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeResolver'][1442500255] = [
	'nodeName' => 'text',
	'priority' => 60,
	'class' => \SGalinski\Tinymce4Rte\Form\Resolver\RichTextNodeResolver::class,
];

// load default PageTS config
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
	'<INCLUDE_TYPOSCRIPT: source="FILE:EXT:tinymce4_rte/Configuration/TypoScript/pageTSConfig.ts">'
);

// load default userTS config
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig(
	'<INCLUDE_TYPOSCRIPT: source="FILE:EXT:tinymce4_rte/Configuration/TypoScript/userTSConfig.ts">'
);

// load default typoscript config
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript(
	'tinymce4_rte',
	'setup',
	'<INCLUDE_TYPOSCRIPT: source="FILE:EXT:tinymce4_rte/Configuration/TypoScript/ImageRendering/setup.ts">',
	'defaultContentRendering'
);

// Registering soft reference parser for img tags in RTE content
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['GLOBAL']['softRefParser']['rtehtmlarea_images'] =
	\SGalinski\Tinymce4Rte\Hook\SoftReferenceHook::class;
