<?php
use TYPO3\CMS\Backend\Controller;

/**
 * Definitions for routes provided by EXT:backend
 * Contains all AJAX-based routes for entry points
 *
 * Currently the "access" property is only used so no token creation + validation is made
 * but will be extended further.
 */
return [
    'rte_insert_image' => [
        'path' => '/rte/insert-image',
        'target' => \SGalinski\Tinymce4Rte\Controller\SelectImageController::class . '::buildImageMarkup',
    ],
	'tinymce4_rte::ajaxPing' => [
		'path' => '/tinymce4_rte/ajaxPing',
		'target' => \SGalinski\Tinymce4Rte\Service\LicensingService::class . '::ajaxPing',
	]
];
