<?php

/**
 * Definitions of routes
 */
return [
	// Register RTE browse links wizard
	'rtehtmlarea_wizard_browse_links' => [
		'path' => '/rte/wizard/link',
		'target' => \SGalinski\Tinymce4Rte\Controller\BrowseLinksController::class . '::mainAction'
	],
	// Register RTE select image wizard
	'rtehtmlarea_wizard_select_image' => [
		'path' => '/rte/wizard/image',
		'target' => \SGalinski\Tinymce4Rte\Controller\SelectImageController::class . '::mainAction'
	],
];
