tinymce.init({
	selector: '.tinymce4_rte',
	browser_spellcheck: true,
	paste_as_text: true,
	remove_script_host: false,
	relative_urls: false,
	autoresize_max_height: 500,
	plugins: [
		'advlist autolink lists charmap hr anchor pagebreak searchreplace wordcount visualblocks visualchars code',
		'nonbreaking save table directionality template paste colorpicker autoresize shy contextmenu fullscreen help',
		'codemirror'
	],
	// Currently not in use typo3image
	toolbar1: 'undo redo | styleselect | bold italic | typo3image typo3link unlink | ' +
		'alignleft aligncenter alignright | bullist numlist outdent indent | fullscreen | help | code',
	external_plugins: {
		codemirror: 'EXT:tinymce4_rte/Resources/Public/Plugins/tinymce-codemirror/plugins/codemirror/plugin.min.js',
		shy: 'EXT:tinymce/Resources/Public/JavaScript/TinymcePlugins/shy/plugin.min.js',
		typo3filemanager: 'EXT:tinymce4_rte/Resources/Public/Plugins/Typo3FileManager/typo3filemanager.min.js',
	},
	// Currently not in use typo3image
	contextmenu: 'typo3link unlink inserttable | cell row column deletetable',
	codemirror: {
		indentOnInit: true,
		config: {
			// provokes problems while doing copy&paste from the source code window
			lineNumbers: false
		},
		path: 'EXT:tinymce4_rte/Resources/Public/Plugins/tinymce-codemirror/plugins/codemirror/CodeMirror/'
	}


// Example for adding/changing style formats
//	,style_formats: [
//		{
//			title: 'Headers', items: [
//			{title: 'Header 2', block: 'h2'},
//			{title: 'Header 3', block: 'h3'},
//			{title: 'Header 4', block: 'h4'}
//		]
//		},
//		{
//			title: 'Inline', items: [
//			{title: 'Bold', icon: "bold", inline: 'strong'},
//			{title: 'Italic', icon: "italic", inline: 'em'},
//			{title: 'Underline', icon: "underline", inline: 'span', styles: {'text-decoration': 'underline'}},
//			{title: 'Strikethrough', icon: "strikethrough", inline: 'span', styles: {'text-decoration': 'line-through'}},
//			{title: 'Superscript', icon: "superscript", inline: 'sup'},
//			{title: 'Subscript', icon: "subscript", inline: 'sub'},
//			{title: 'Code', icon: "code", inline: 'code'}
//		]
//		},
//		{
//			title: 'Blocks', items: [
//			{title: 'Paragraph', block: 'p'},
//			{title: 'Blockquote', block: 'blockquote'},
//			{title: 'Div', block: 'div', format: 'greenBox'},
//			{title: 'Pre', block: 'pre'}
//		]
//		},
//		{
//			title: 'Alignment', items: [
//			{title: 'Left', icon: "alignleft", block: 'div', styles: {'text-align': 'left'}},
//			{title: 'Center', icon: "aligncenter", block: 'div', styles: {'text-align': 'center'}},
//			{title: 'Right', icon: "alignright", block: 'div', styles: {'text-align': 'right'}}
//		]
//		}
//	],
//	Activate this, if you've a lot of "style_formats", which could just be applied to specific tags. This can bring a
//	better structure, but if you don't have much, then this option will just confuse the customer.
//	style_formats_autohide: true,
//	formats: {
//		greenBox: {
//			block: 'div',
//			wrapper: 1,
//			remove: 'all',
//			classes: 'box-green'
//		}
//	}
});
